<?php

namespace App\Controller\Api;

use App\Controller\BaseAbstractController;
use App\Entity\User;
use Cassandra\Exception\UnauthorizedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/admin")
 */
class ApiAdminController extends BaseAbstractController
{
    /**
     * @Route("/detail", name="api_admin_detail", methods={"POST"})
     * @param User $admin
     * @return JsonResponse
     */
    public function detail()
    {
        $admin = $this->getCurrentUser();

        $this->denyAccessUnlessGranted('view', $admin);

        if( !$this->isUserAuthenticated() ) {
            Throw new UnauthorizedException();
        }

        if (!$this->getAuthUser()->hasRole('ROLE_ADMIN')) {
            Throw new AccessException();
        }

        return new JsonResponse($this->serialize($admin), 200);
    }

    protected function serialize(User $admin)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($admin, 'json');

        return $json;
    }

}
