<?php

namespace App\Controller\Api;

use App\Controller\BaseAbstractController;
use App\Entity\User;
use Cassandra\Exception\UnauthorizedException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Route("/user")
 */
class ApiUserController extends BaseAbstractController
{

    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/detail", name="api_user_detail", methods={"POST"})
     * @return JsonResponse
     */
    public function detail()
    {
        $user = $this->getCurrentUser();

        $this->denyAccessUnlessGranted('view', $user);

        if( !$this->isUserAuthenticated() ) {
            Throw new UnauthorizedException();
        }

        if (!$this->getAuthUser()->hasRole('ROLE_USER')) {
            Throw new AccessException();
        }

        return new JsonResponse($this->serialize($user), 200);
    }

    /**
     * @Route("/verifycaptcha", name="api_user_verifycaptcha", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyReCaptcha(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        $user = $this->getCurrentUser();

        $secret = $this->params->get('google_recaptcha_site_key');
        $remoteIp = null;
        $gRecaptchaResponse = $data['token'];

        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp = $recaptcha->setExpectedHostname('recaptcha-demo.appspot.com')->verify($gRecaptchaResponse, $remoteIp);
        if ($resp->isSuccess()) {
            // Verified!
            return new JsonResponse(['success' => true], 200);
        } else {
            $errors = $resp->getErrorCodes();
            return new JsonResponse(['success' => false], 200);
        }
    }

    protected function serialize(User $user)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($user, 'json');

        return $json;
    }

}
