<?php

namespace App\Controller\Api;

use App\Controller\BaseAbstractController;
use App\Entity\User;
use Cassandra\Exception\UnauthorizedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/contenteditor")
 */
class ApiContentEditorController extends BaseAbstractController
{
    /**
     * @Route("/detail", name="api_contenteditor_detail", methods={"POST"})
     * @param User $contenteditor
     * @return JsonResponse
     */
    public function detail()
    {
        $contenteditor = $this->getCurrentUser();

        $this->denyAccessUnlessGranted('view', $contenteditor);

        if( !$this->isUserAuthenticated() ) {
            Throw new UnauthorizedException();
        }

        if (!$this->getAuthUser()->hasRole('ROLE_EDIT')) {
            Throw new AccessException();
        }

        return new JsonResponse($this->serialize($contenteditor), 200);
    }

    protected function serialize(User $contenteditor)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->serialize($contenteditor, 'json');

        return $json;
    }

}
