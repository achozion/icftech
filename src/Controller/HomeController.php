<?php

namespace App\Controller;

use Cassandra\Exception\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseAbstractController
{

    public function home(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $twig = 'home.html.twig';

        return $this->render($twig, [
            'navigation' => $this->declareMenu(),
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

}