<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PropertyAccess\Exception\AccessException;

abstract class BaseAbstractController extends AbstractController
{
    protected $menu = [
        "contenteditor" => ["href"=>"api/contenteditor/detail","caption"=>"contenteditor"],
        "user" => ["href"=>"api/user/detail","caption"=>"user"],
        "admin" => ["href"=>"api/admin/detail","caption"=>"admin"],
    ];

    protected function isUserAuthenticated(): bool
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) )
        {
            return true;
        }

        return false;
    }

    protected function getAuthUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    protected function getCurrentUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The Security Bundle is not registered in your application.');
        }
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }
        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }
        return $user;
    }

    protected function declareMenu()
    {
        return $this->menu;

        $menu = array();

        if (!method_exists($this->getAuthUser(), "hasRole")) {
            return $menu;
        }

        if ($this->getAuthUser()->hasRole('ROLE_USER')) {
            $menu["user"] = $this->menu["user"];
        }

        if ($this->getAuthUser()->hasRole('ROLE_ADMIN')) {
            $menu["admin"] = $this->menu["admin"];
        }

        if ($this->getAuthUser()->hasRole('ROLE_EDIT')) {
            $menu["contentedit"] = $this->menu["contentedit"];
        }

        return $menu;
    }

}
