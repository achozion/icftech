Mellékeltem a szakmai leirást is:
Megoldásom egy REST API s JWT tokenes authentikácio
Külön van választva a back end és a front end, a kommunikácio rest API s hivásokkal történik

clins oldalon npm van
backend oldalon composer

Docker konténer szolgáltatja a környezetet
docker compose fájl mellékeve
docker-compose up -d
login / enter docker:
 docker exec -it icftech-mysql mysql -usf4_user -psf4_pw

Az oldalon a menüpontokkal lehet navigálni
de CURL hivásokkal is le lehet kérni pl az user adatokat (lent részletezve)
Run build npm
./node_modules/.bin/encore dev
./node_modules/.bin/encore dev --watch

új felhasználót lehet regisztrálni is CURL hivással
curl create user / curl register
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/auth/register -d '{"username":"akos","password":"akos", "email":"achozion@gmail.com"}'

curl login 
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/auth/login -d '{"username":"akos","password":"akos"}'

curl get user data with JWT auth token
curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1ODY4MDU2MTQsImV4cCI6MTU4NjgwOTIxNCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoicGF0YXRhIn0.lAb_HJhlkUklULo9pPU46vp42N24yXNNhiRF2HcrjCbOsvTb8Gc4osJFdsHvBTkamvHvNIZAdDVNuyYh_eKK1quPy0llmTmTT3QVZ6YY4mNLtlku-YI21voO1ob9IBCoGBlS4xiZQtVSMF5KfRKmWuN60DS17Jl5Cucl6-ENtuaelQJa-2Hppa4ImStgNxGsxLKwkaciTP_S02roONnynTIbDrVEe2q5U1kRqbFz32WphNKjafXDXNoFINuHKFUTWKw16CcI-uRCl8WYtRUOXthAyCLiXYw4B-DKBvQ4lZ67a0U8cY0qvs6kxHn5iJLlna7T0t-iVoPopbizLAGzxck3njkWkOy4vraRHFAKSFHv1edsSMkiS1q5VEJ2O-RjJDf7HMR81YJv2QLj6nV4VGBctcpoy90GyUwsoB-RbcRURe8fidZuDHf51r8BxfpKC_XHlyDScur2SEPOgZ4W46bXIsw8qEhXk5u9-lbeSkrCN_8ck6m7VsGsskg66GeGxa3UrkR3oSD8CARt2RDy5YK9TztmsUQI5h5Rijv1MY_YP7S-j3nicxoMpI1X28yt2p2ukxfiUBvf89PRlheJscMz8SoCheDw0jQYVXjjZ2xuaIQiimOWdUdmmS-MZV796E_3Lr0ATvGE1jao4Lp3Afrj4YwHh_dOyPlY9fWW7fI" http://localhost:8000/api/user/1

commands:
add permission / role to user
php bin/console fos:user:promote testuser ROLE_ADMIN

remove permission / role from user
php bin/console fos:user:demote testuser ROLE_ADMIN	

page
http://localhost:8000

api docs
http://localhost:8000/api/doc

Google Recaptca:Ezeket be kell illeszteni az adott helyre (a sajétomat kitöröltem)
Site key: 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI
Secret key: 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe

"With the following test keys, you will always get No CAPTCHA and all verification requests will pass.

Köszönöm a lehetőséget, hogy megoldhattam a tsztfeladatot
Üdvözlettel:Varga Ákos
achozion@gmail.com



